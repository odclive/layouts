# layouts

ODC Live's layout tool.

## Installation

Make sure you have the GIMP script in the correct place in order for GIMP to
read it. You should also be sure to have the fonts files on your file system
for everything to run as expected.
