#!/bin/sh

file="image.png"
date="01/12"
artist="Great DJ"
title="Awesome show"
color="black"

# gimp -i \
#   -b "(odc-template \"$file\" \"$date\" \"$artist\" \"$title\" \"$color\")" \
#   -b '(gimp-quit 0)'

gimp -i \
  -b "(odc-template \"$1\" \"$2\" \"$3\" \"$4\" \"$5\")" \
  -b '(gimp-quit 0)'

