(define (odc-template filename date artist title color)
  
  (srand (car (gettimeofday))) ; seed replacing
  
  (let* (
         (image (car (gimp-file-load RUN-NONINTERACTIVE filename filename)))
         (sourceWidth (car (gimp-image-width image)))
         (sourceHeight (car (gimp-image-height image)))
         (outputDim (if (< sourceWidth sourceHeight) sourceWidth sourceHeight))
         (textColor (if (equal? color "black") '(255 255 255) '(0 0 0))) 
         (layoutName (if (equal? color "black") "layouts/lay_black.png" "layouts/lay_white.png")) 
         )

    ; scaling image to layout's dimension
    (gimp-image-crop image outputDim outputDim 0 0)
    (gimp-image-scale image 800 800)

    (let* (
           (y 0)
           (yMax (+ 4 (random 4)))
           (imageLayer (car (gimp-image-get-active-layer image)))
           (layoutImage (car (gimp-file-load RUN-NONINTERACTIVE layoutName layoutName)))
           (layoutLayer (car (gimp-image-get-active-layer layoutImage)))
           ; Create a new layer
           (layer (car (gimp-layer-new-from-drawable layoutLayer image)))
           )

      ; add drawable to template
      (gimp-image-insert-layer image layer 0 -1)
      ; flatten image
      (gimp-image-flatten image)
      ; drawable id has changed, thus the need to reset drawable
      (set! imageLayer (car (gimp-image-get-active-layer image)))

      (gimp-context-set-foreground textColor)
      (gimp-text-fontname image imageLayer 29 44 date 0 TRUE 31 1 "Retina MP Medium")
      (gimp-text-fontname image imageLayer 141 706 artist 0 TRUE 40 1 "Retina MP Medium")
      (gimp-text-fontname image imageLayer 141 751 title 0 TRUE 31 1 "Retina MP Medium")
      (gimp-image-flatten image)
      ; drawable id has changed, thus the need to reset drawable
      (set! imageLayer (car (gimp-image-get-active-layer image)))

      (while (< y yMax)
             (let*(
                   (shapeNumber (number->string (random 31))) 
                   (shapeColor (if (equal? color "black") "shapes/white/Fichier " "shapes/black/Fichier ")) 
                   (shapeName (string-append shapeColor shapeNumber))
                   (shapeName (string-append shapeName ".png"))
                   (shapeImage (car (gimp-file-load RUN-NONINTERACTIVE shapeName shapeName)))
                   (shapeLayout (car (gimp-image-get-active-layer shapeImage)))
                   (shapeWidth (car (gimp-image-width shapeImage)))
                   (shapeHeight (car (gimp-image-height shapeImage)))
                   ; Create a new layer
                   (shapeLayer (car (gimp-layer-new-from-drawable shapeLayout image)))
                    
                   (posX (+ 0 (random 800))) 
                   (posY (+ 0 (random 800)))
                   (shapeSize (+ 75 (random 30)))
                   )
             
            ; add shape to image
            (gimp-image-insert-layer image shapeLayer 0 -1)
            ; x0 y0 x1 y1
            (gimp-item-transform-scale shapeLayer
                                       (- posX (* (/ shapeWidth shapeHeight) shapeSize))
                                       (- posY shapeSize)
                                       posX
                                       posY)
            (gimp-item-transform-rotate shapeLayer (random 360) 1 0 0)
            (gimp-image-flatten image)
            (gimp-image-delete shapeImage)
            (set! imageLayer (car (gimp-image-get-active-layer image)))
            )
            (set! y (+ y 1)))


      (file-jpeg-save RUN-NONINTERACTIVE image imageLayer "/tmp/output.jpg" "/tmp/output.jpg" 1 0 1 1 "" 2 1 0 0)

      ; clear buffer, avoiding leaks
      (gimp-image-delete layoutImage)
      (gimp-image-delete image)
    )
  )
)
